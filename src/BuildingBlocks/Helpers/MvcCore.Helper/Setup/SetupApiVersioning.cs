﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;

namespace MvcCore.Helper.Setup
{
    public static class SetupApiVersioning
    {
        public static IServiceCollection AddCustomApiVersioning(this IServiceCollection services) =>
            services
                .AddApiVersioning(opt =>
                {
                    opt.ReportApiVersions = true;
                    opt.AssumeDefaultVersionWhenUnspecified = true;
                    opt.DefaultApiVersion = new ApiVersion(1, 0);
                    opt.ApiVersionReader =
                        ApiVersionReader.Combine(
                            new QueryStringApiVersionReader("version"),
                            new HeaderApiVersionReader("version")
                            {
                                HeaderNames = { "api-version", "x-ms-version" }
                            });
                });
    }
}