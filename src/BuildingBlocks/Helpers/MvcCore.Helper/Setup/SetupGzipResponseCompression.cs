﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using System.IO.Compression;
using System.Linq;

namespace MvcCore.Helper.Setup
{
    public static class SetupGzipResponseCompression
    {
        public static IServiceCollection AddGzipResponseCompression(this IServiceCollection services,
            string[] mimeTypes = null, CompressionLevel compressionLevel = CompressionLevel.Optimal)
        {
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();

                if (mimeTypes != null)
                {
                    options.MimeTypes =
                        ResponseCompressionDefaults.MimeTypes.Concat(mimeTypes);
                }

            });

            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = compressionLevel;
            });

            return services;
        }
    }
}