﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;

namespace MvcCore.Helper.Setup
{
    public static class SetupCustomMvc
    {
        public static IServiceCollection AddCustomMvc(this IServiceCollection services, IConfiguration configuration,
            Type globalFilterType = null)
        {
            services
                .AddMvc(opt =>
                {
                    var routePrefix = configuration["GlobalApiPrefix"];
                    if (!string.IsNullOrEmpty(routePrefix))
                    {
                        opt.UseCentralRoutePrefix(new RouteAttribute(routePrefix));
                    }

                    if (globalFilterType != null)
                    {
                        opt.Filters.Add(globalFilterType);
                    }
                })
                .AddControllersAsServices()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var detail = new ValidationProblemDetails(context.ModelState)
                    {
                        Instance = context.HttpContext.Request.Path,
                        Status = StatusCodes.Status400BadRequest,
                        Type = "https://asp.net/core",
                        Detail = "Please refer to the errors property for additional details."
                    };

                    return new BadRequestObjectResult(detail)
                    {
                        ContentTypes = { "application/problem+json", "application/problem+xml" }
                    };
                };
            })
                .AddOptions()
                .AddGzipResponseCompression()
                .AddCustomApiVersioning()
                .AddHttpContextAccessor();


            return services;
        }
    }
}