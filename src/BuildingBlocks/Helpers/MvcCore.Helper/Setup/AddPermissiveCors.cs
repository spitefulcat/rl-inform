﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace MvcCore.Helper.Setup
{
    public static class SetupPermissiveCors
    {
        public static IServiceCollection AddPermissiveCors(this IServiceCollection services) =>
            services
                .AddCors(options =>
                {
                    options.AddPolicy("CorsPolicy", builder => builder
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .WithOrigins("http://localhost:4200")
                        .AllowCredentials()
                    );
                });

        public static IApplicationBuilder UsePermissiveCors(this IApplicationBuilder app)
            => app.UseCors("CorsPolicy");
    }
}