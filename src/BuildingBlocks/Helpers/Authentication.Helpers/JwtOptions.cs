﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Authentication.Helpers
{
    public class JwtOptions
    {
        public string Issuer { get; set; }

        public string Subject { get; set; }

        public string Audience { get; set; }

        public int Expire { get; set; }

        public string SecretKey { get; set; }

        public SymmetricSecurityKey GenerateSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecretKey));
        }
    }
}