﻿using System.Collections.Generic;

namespace Authentication.Helpers
{
    public interface IJwtFactory
    {
        object GenerateToken(int idUser, IEnumerable<string> roles = null);
    }
}