﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Authentication.Helpers
{
    public class JwtFactory : IJwtFactory
    {
        private readonly JwtOptions JwtOptions;

        public JwtFactory(IOptions<JwtOptions> jwtOptions)
        {
            JwtOptions = jwtOptions.Value;
        }

        public object GenerateToken(int idUser, IEnumerable<string> roles = null)
        {
            var dtUtcNow = DateTime.UtcNow;
            var expires = dtUtcNow.AddMinutes(JwtOptions.Expire);

            var claims = new List<Claim>
            {
              new Claim(ClaimTypes.NameIdentifier,idUser.ToString()),
              new Claim(JwtRegisteredClaimNames.Sub, idUser.ToString()),
              new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
              new Claim(JwtRegisteredClaimNames.Iat, expires.ToUniversalTime().ToString(CultureInfo.InvariantCulture), ClaimValueTypes.Integer64)
            };

            if (roles != null)
            {
                foreach (var role in roles)
                    claims.Add(new Claim(ClaimTypes.Role, role));
            }


            var creds = new SigningCredentials(JwtOptions.GenerateSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(JwtOptions.Issuer,
              JwtOptions.Audience,
              claims,
              dtUtcNow,
              expires,
              creds);

            return new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expires
            };
        }
    }
}