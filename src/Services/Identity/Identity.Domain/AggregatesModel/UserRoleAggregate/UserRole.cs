﻿using Identity.Domain.AggregatesModel.RoleAggregate;
using Identity.Domain.AggregatesModel.UserAggregate;
using Identity.Domain.SeedWork;

namespace Identity.Domain.AggregatesModel.UserRoleAggregate
{
    public class UserRole : Entity, IAggregateRoot
    {
        public int IdRole { get; set; }
        public Role Role { get; set; }

        public int IdUser { get; set; }
        public User User { get; set; }

        public UserRole(int idRole, int idUser)
        {
            IdRole = idRole;
            IdUser = idUser;
        }
    }
}