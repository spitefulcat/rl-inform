﻿using Identity.Domain.SeedWork;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Identity.Domain.AggregatesModel.UserRoleAggregate
{
    public interface IUserRoleRepository : IBaseRepository<UserRole>
    {
        Task<UserRole> GetById(int id);

        void Create(UserRole userRole);

        void CreateAll(IEnumerable<UserRole> userRoles);

        void Delete(int idUser);
    }
}