﻿using Identity.Domain.SeedWork;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Domain.AggregatesModel.UserAggregate
{
    public interface IUserRepository : IBaseRepository<User>
    {
        IQueryable<User> GetAll();

        Task<User> GetById(int id);

        Task<User> GetByLogin(string login);

        void Create(User user);

        void Update(User user);

        Task Delete(int id);
    }
}