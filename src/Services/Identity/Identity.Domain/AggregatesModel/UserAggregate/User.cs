﻿using Identity.Domain.AggregatesModel.UserRoleAggregate;
using Identity.Domain.SeedWork;
using System.Collections.Generic;

namespace Identity.Domain.AggregatesModel.UserAggregate
{
    public class User : Entity, IAggregateRoot
    {
        public string Login { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string PasswordHash { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }


        public User() { }

        public User(string login, string name, string email, string passwordHash)
        {
            Login = login;
            Name = name;
            Email = email;
            PasswordHash = passwordHash;
        }
    }
}