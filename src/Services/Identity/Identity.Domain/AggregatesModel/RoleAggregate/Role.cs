﻿using Identity.Domain.AggregatesModel.UserRoleAggregate;
using Identity.Domain.Exceptions;
using Identity.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Identity.Domain.AggregatesModel.RoleAggregate
{
    public class Role : Enumeration
    {
        public static Role Administrator = new Role(1, nameof(Administrator));
        public static Role DirectoryEditor = new Role(2, nameof(DirectoryEditor));
        public static Role Customer = new Role(3, nameof(Customer));

        public Role(int id, string name)
            : base(id, name)
        {
        }

        public static IEnumerable<Role> List() =>
            new[] { Administrator, DirectoryEditor, Customer };

        public virtual ICollection<UserRole> UserRoles { get; set; }

        public static Role FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name));

            if (state == null)
            {
                throw new IdentityDomainException($"Possible values for Role: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }

        public static Role From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
            {
                throw new IdentityDomainException($"Possible values for Role: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }
    }
}