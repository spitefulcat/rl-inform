﻿namespace Identity.Domain.SeedWork
{
    public interface IBaseRepository<T> where T : IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
    }
}