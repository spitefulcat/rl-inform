﻿using Identity.Domain.AggregatesModel.UserAggregate;
using Identity.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Infrastructure.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IdentityDbContext _context;

        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _context;
            }
        }

        public UserRepository(IdentityDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IQueryable<User> GetAll()
        {
            return _context.Users.Include(x => x.UserRoles)
                .ThenInclude(x => x.Role)
                .AsNoTracking()
                .OrderBy(c => c.Login);
        }

        public async Task<User> GetById(int id)
        {
            return await _context.Users.Include(x => x.UserRoles)
                .ThenInclude(x => x.Role)
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<User> GetByLogin(string login)
        {
            return await _context.Users.Include(x => x.UserRoles)
                .ThenInclude(x => x.Role)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Login == login);
        }

        public void Create(User user)
        {
            _context.Users.Add(user);
        }

        public void Update(User user)
        {
            _context.Users.Update(user);
            _context.Entry(user).State = EntityState.Modified;
        }

        public async Task Delete(int id)
        {
            var user = await GetById(id);
            if (_context.Entry(user).State == EntityState.Detached)
            {
                _context.Attach(user);
            }

            _context.Users.Remove(user);
        }
    }
}