﻿using Identity.Domain.AggregatesModel.UserRoleAggregate;
using Identity.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Infrastructure.Repository
{
    public class UserRoleRepository : IUserRoleRepository
    {
        private readonly IdentityDbContext _context;
        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _context;
            }
        }


        public UserRoleRepository(IdentityDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<UserRole> GetById(int id)
        {
            return await _context.UserRoles.AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        public void Create(UserRole userRole)
        {
            _context.UserRoles.Add(userRole);
        }

        public void CreateAll(IEnumerable<UserRole> userRoles)
        {
            _context.UserRoles.AddRange(userRoles);
        }

        public void Delete(int idUser)
        {
            var userRoles = _context.UserRoles.Local
                .Where(x => x.IdUser == idUser);

            foreach (var userRole in userRoles)
            {
                if (_context.Entry(userRole).State == EntityState.Detached)
                {
                    _context.Attach(userRole);
                }

                _context.Remove(userRole);
            }
        }
    }
}