﻿using Identity.Domain.AggregatesModel.UserRoleAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.Infrastructure.EntityConfigurations
{
    class UserRoleEntityTypeConfiguration : IEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> userRolesConfiguration)
        {
            userRolesConfiguration.ToTable("UserRoles");

            userRolesConfiguration.HasKey(o => o.Id);

            userRolesConfiguration.Ignore(b => b.DomainEvents);

            userRolesConfiguration.Property<int>("IdUser")
                .IsRequired();

            userRolesConfiguration.Property<int>("IdRole")
               .IsRequired();

            userRolesConfiguration.HasOne(p => p.Role)
                .WithMany(b => b.UserRoles)
                .HasForeignKey(p => p.IdRole);

            userRolesConfiguration.HasOne(p => p.User)
              .WithMany(b => b.UserRoles)
              .HasForeignKey(p => p.IdUser);
        }
    }
}
