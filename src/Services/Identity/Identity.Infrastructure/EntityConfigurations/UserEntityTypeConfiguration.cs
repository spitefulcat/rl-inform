﻿using Identity.Domain.AggregatesModel.UserAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.Infrastructure.EntityConfigurations
{
    class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> identityConfiguration)
        {
            identityConfiguration.ToTable("Users");

            identityConfiguration.HasKey(o => o.Id);

            identityConfiguration.HasIndex(x => x.Login)
               .IsUnique();

            identityConfiguration.Ignore(b => b.DomainEvents);

            identityConfiguration.Property<string>("Login")
                .IsRequired()
                .HasMaxLength(50);
            identityConfiguration.Property<string>("Name")
                .IsRequired()
                .HasMaxLength(50);
            identityConfiguration.Property<string>("Email")
                .IsRequired()
                .HasMaxLength(255);

            var navigation = identityConfiguration.Metadata.FindNavigation(nameof(User.UserRoles));
            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}