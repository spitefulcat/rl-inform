﻿using Identity.Domain.AggregatesModel.RoleAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.Infrastructure.EntityConfigurations
{
    class RoleEntityTypeConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> roleConfiguration)
        {
            roleConfiguration.ToTable("Roles");

            roleConfiguration.HasKey(o => o.Id);

            roleConfiguration.Property(o => o.Id)
                .ValueGeneratedNever()
                .IsRequired();

            roleConfiguration.Property(o => o.Name)
                .HasMaxLength(50)
                .IsRequired();
        }
    }
}