﻿using Identity.API.Application.Commands;
using Identity.API.Application.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.API.Controllers
{
    [AllowAnonymous]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("[controller]")]
    public class AccountsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IIdentityQueries _identityQueries;

        public AccountsController(
            IMediator mediator,
            IIdentityQueries identityQueries)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _identityQueries = identityQueries ?? throw new ArgumentNullException(nameof(identityQueries));
        }

        // GET: api/v1/accounts/user/1
        [Route("user/{id:int}")]
        [HttpGet]
        public async Task<ActionResult> GetUserAsync([FromRoute] int id)
        {
            var userVM = await _identityQueries.GetUserByIdAsync(id);

            return Ok(userVM);
        }

        // GET: api/v1/accounts/users
        [Route("users")]
        [HttpGet]
        public ActionResult GetUsersAsync()
        {
            var userVM = _identityQueries.GetUsers();

            return Ok(userVM);
        }

        // POST: api/v1/accounts/create
        [Route("create")]
        [HttpPost]
        public async Task<ActionResult> CreateUserAsync([FromBody] CreateUserCd createUserCd)
        {
            var user = await _identityQueries.GetUserByLoginAsync(createUserCd.Login);
            if (user != null)
            {
                return BadRequest();
            }

            var commandResult = await _mediator.Send(createUserCd);
            if (!commandResult)
            {
                return BadRequest();
            }

            if (createUserCd.Roles == null || createUserCd.Roles.Count() <= 0)
            {
                user = await _identityQueries.GetUserByLoginAsync(createUserCd.Login);
                if (user == null)
                {
                    return BadRequest();
                }

                return CreatedAtAction("GetUserAsync", new { id = user.Id }, user);
            }


            user = await _identityQueries.GetUserByLoginAsync(createUserCd.Login);
            if (user == null)
            {
                return BadRequest();
            }

            var command = new CreateUserRoleCd(user.Id, createUserCd.Roles);
            commandResult = await _mediator.Send(command);
            if (!commandResult)
            {
                return BadRequest();
            }

            user = await _identityQueries.GetUserByLoginAsync(createUserCd.Login);
            if (user == null)
            {
                return BadRequest();
            }

            return CreatedAtAction("GetUserAsync", new { id = user.Id }, user);
        }

        // DELETE: api/v1/accounts/delete/1
        [Route("delete/{id:int}")]
        [HttpDelete]
        public async Task<ActionResult> DeleteUserAsync([FromRoute] int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var user = await _identityQueries.GetUserByIdAsync(id);
            if (user == null)
            {
                return BadRequest();
            }

            var command = new DeleteUserCd(id);
            var commandResult = await _mediator.Send(command);
            if (!commandResult)
            {
                return BadRequest();
            }

            return NoContent();
        }

        // PUT: api/v1/accounts/update/1
        [Route("update/{id:int}")]
        [HttpPut]
        public async Task<ActionResult> UpdateUserAsync([FromRoute] int id, [FromBody] UpdateUserCd updateUserCd)
        {
            if (id != updateUserCd.Id)
            {
                return BadRequest();
            }

            var user = await _identityQueries.GetUserByIdAsync(id);
            if (user == null)
            {
                return BadRequest();
            }

            var commandResult = await _mediator.Send(updateUserCd);
            if (!commandResult)
            {
                return BadRequest();
            }


            if (updateUserCd.Roles == user.Roles)
            {
                return CreatedAtAction("GetUserAsync", new { id = user.Id });
            }


            var command = new UpdateUserRoleCd(updateUserCd.Id, updateUserCd.Roles);
            commandResult = await _mediator.Send(command);
            if (!commandResult)
            {
                return BadRequest();
            }

            return CreatedAtAction("GetUserAsync", new { id = user.Id });
        }

        // GET: api/v1/accounts/roles
        [Route("roles")]
        [HttpGet]
        public ActionResult GetRolesAsync()
        {
            var roles = _identityQueries.GetRoles();

            return Ok(roles);
        }
    }
}