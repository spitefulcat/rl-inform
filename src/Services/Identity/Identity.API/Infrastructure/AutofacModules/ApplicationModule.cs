﻿using Authentication.Helpers;
using Autofac;
using Identity.API.Application.Queries;
using Identity.Domain.AggregatesModel.UserAggregate;
using Identity.Domain.AggregatesModel.UserRoleAggregate;
using Identity.Infrastructure.Repository;

namespace Identity.API.Infrastructure.AutofacModules
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<JwtFactory>().As<IJwtFactory>()
                .InstancePerLifetimeScope();

            builder.RegisterType<IdentityQueries>().As<IIdentityQueries>()
               .InstancePerLifetimeScope();

            builder.RegisterType<UserRepository>().As<IUserRepository>()
               .InstancePerLifetimeScope();

            builder.RegisterType<UserRoleRepository>().As<IUserRoleRepository>()
             .InstancePerLifetimeScope();
        }
    }
}