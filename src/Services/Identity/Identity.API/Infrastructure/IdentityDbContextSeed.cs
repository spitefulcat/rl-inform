﻿using Authentication.Helpers;
using Identity.Domain.AggregatesModel.RoleAggregate;
using Identity.Domain.AggregatesModel.UserAggregate;
using Identity.Domain.AggregatesModel.UserRoleAggregate;
using Identity.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Retry;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.API.Infrastructure
{
    public class IdentityDbContextSeed
    {
        public async Task SeedAsync(IdentityDbContext context, ILogger<IdentityDbContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(IdentityDbContextSeed));

            await policy.ExecuteAsync(async () =>
            {
                using (context)
                {
                    context.Database.Migrate();

                    var roles = GetDefaultRoles().ToList();
                    if (context.Roles.Any())
                    {
                        foreach (var role in roles)
                        {
                            if (context.Roles.All(x => x.Name != role.Name))
                            {
                                context.Roles.Add(role);

                                await context.SaveChangesAsync();
                            }
                        }
                    }
                    else
                    {
                        context.Roles.AddRange(roles);

                        await context.SaveChangesAsync();
                    }


                    if (context.Users.Any())
                    {
                        if (context.Users.All(x => x.Login != "admin"))
                        {
                            context.Users.Add(GetDefaultAdmin());

                            await context.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        context.Users.AddRange(GetDefaultAdmin());

                        await context.SaveChangesAsync();
                    }

                    var idDefaultRole = context.Roles.Where(x => x.Name == Role.Administrator.Name).Select(y => y.Id).FirstOrDefault();
                    var idDefaultUser = context.Users.Where(x => x.Login == "admin").Select(x => x.Id).FirstOrDefault();
                    if (context.Users.Any())
                    {
                        if (context.UserRoles.All(x => x.IdRole != idDefaultRole && x.IdUser != idDefaultUser))
                        {
                            var userRole = new UserRole(idDefaultRole, idDefaultUser);
                            context.UserRoles.Add(userRole);

                            await context.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        var userRole = new UserRole(idDefaultRole, idDefaultUser);
                        context.UserRoles.Add(userRole);

                        await context.SaveChangesAsync();
                    }
                }
            });
        }

        private User GetDefaultAdmin()
        {
            var passwordHash = PasswordHasher.GetHash("Superadmin");
            var user = new User(login: "admin", name: "Incognito", email: "admin@microsoft.com", passwordHash);

            return user;
        }

        private IEnumerable<Role> GetDefaultRoles()
        {
            return new List<Role>()
            {
                Role.Administrator,
                Role.DirectoryEditor,
                Role.Customer
            };
        }

        private AsyncRetryPolicy CreatePolicy(ILogger<IdentityDbContextSeed> logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>().
                WaitAndRetryAsync(
                    retryCount: retries,
                    sleepDurationProvider: retry => TimeSpan.FromSeconds(5),
                    onRetry: (exception, timeSpan, retry, ctx) =>
                    {
                        logger.LogWarning(exception, "[{prefix}] Exception {ExceptionType} with message {Message} detected on attempt {retry} of {retries}", prefix, exception.GetType().Name, exception.Message, retry, retries);
                    }
                );
        }
    }
}