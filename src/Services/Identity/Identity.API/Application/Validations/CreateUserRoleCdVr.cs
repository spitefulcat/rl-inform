﻿using FluentValidation;
using Identity.API.Application.Commands;
using Microsoft.Extensions.Logging;

namespace Identity.API.Application.Validations
{
    public class CreateUserRoleCdVr : AbstractValidator<CreateUserRoleCd>
    {
        public CreateUserRoleCdVr(ILogger<CreateUserRoleCdVr> logger)
        {
            RuleFor(command => command.IdUser).NotEmpty();

            RuleFor(command => command.Roles).NotEmpty();

            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }
    }
}