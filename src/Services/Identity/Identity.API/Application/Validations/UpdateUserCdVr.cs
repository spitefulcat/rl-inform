﻿using FluentValidation;
using Identity.API.Application.Commands;
using Microsoft.Extensions.Logging;

namespace Identity.API.Application.Validations
{
    public class UpdateUserCdVr : AbstractValidator<UpdateUserCd>
    {
        public UpdateUserCdVr(ILogger<UpdateUserCdVr> logger)
        {
            RuleFor(command => command.Id).NotEmpty();

            RuleFor(command => command.Name).NotEmpty();

            RuleFor(command => command.Email).NotEmpty().EmailAddress();


            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }
    }
}