﻿using FluentValidation;
using Identity.API.Application.Commands;
using Microsoft.Extensions.Logging;

namespace Identity.API.Application.Validations
{
    public class UpdateUserRoleCdVr : AbstractValidator<UpdateUserRoleCd>
    {
        public UpdateUserRoleCdVr(ILogger<UpdateUserRoleCdVr> logger)
        {
            RuleFor(command => command.IdUser).NotEmpty();

            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }
    }
}