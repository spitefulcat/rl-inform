﻿using FluentValidation;
using Identity.API.Application.Commands;
using Microsoft.Extensions.Logging;

namespace Identity.API.Application.Validations
{
    public class CreateUserCdVr : AbstractValidator<CreateUserCd>
    {
        public CreateUserCdVr(ILogger<CreateUserCdVr> logger)
        {
            RuleFor(command => command.Login).NotEmpty();

            RuleFor(command => command.Name).NotEmpty();

            RuleFor(command => command.Email).NotEmpty().EmailAddress();

            RuleFor(command => command.Password).NotEmpty()
                .Equal(x => x.ConfirmPassword)
                .WithMessage("Password does not matches")
                .Matches("[A-Z]")
                .WithMessage("a minimum of 1 upper case letter [A-Z]")
                .Matches("[a-z]")
                .WithMessage("Passwords must contain: a minimum of 1 lower case letter [a-z]")
                .Matches("[0-9]")
                .WithMessage("Passwords must contain: a minimum of 1 numeric character")
                .MinimumLength(8)
                .WithMessage("Passwords must be at least 8 characters in length.");

            RuleFor(command => command.ConfirmPassword).NotEmpty();

            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }
    }
}