﻿using Identity.Domain.AggregatesModel.UserAggregate;
using System.Collections.Generic;
using System.Linq;

namespace Identity.API.Application.Queries
{
    public class UserVM
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public IEnumerable<string> Roles { get; set; }


        public UserVM(User user)
        {
            Id = user.Id;
            Login = user.Login;
            Name = user.Name;
            Email = user.Email;
            Roles = user.UserRoles.Select(x => x.Role.Name);
        }
    }
}