﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Identity.API.Application.Queries
{
    public interface IIdentityQueries
    {
        Task<UserVM> GetUserByIdAsync(int id);

        Task<UserVM> GetUserByLoginAsync(string login);

        IEnumerable<UserVM> GetUsers();

        IEnumerable<string> GetRoles();
    }
}