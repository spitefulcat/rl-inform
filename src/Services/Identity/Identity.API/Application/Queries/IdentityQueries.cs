﻿using Identity.Domain.AggregatesModel.RoleAggregate;
using Identity.Domain.AggregatesModel.UserAggregate;
using Identity.Infrastructure;
using Identity.Infrastructure.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.API.Application.Queries
{
    public class IdentityQueries : IIdentityQueries
    {
        private readonly IdentityDbContext _context;

        private readonly IUserRepository _identityRepository;


        public IdentityQueries(IdentityDbContext context)
        {
            _context = context;
            _identityRepository = new UserRepository(_context);
        }

        public async Task<UserVM> GetUserByIdAsync(int id)
        {
            var user = await _identityRepository.GetById(id);
            if (user == null)
            {
                return null;
            }
            var userVM = new UserVM(user);

            return userVM;
        }

        public async Task<UserVM> GetUserByLoginAsync(string login)
        {
            var user = await _identityRepository.GetByLogin(login);
            if (user == null)
            {
                return null;
            }

            var userVM = new UserVM(user);

            return userVM;
        }

        public IEnumerable<UserVM> GetUsers()
        {
            var users = _identityRepository.GetAll();
            var usersVM = new List<UserVM>();

            foreach (var user in users)
            {
                usersVM.Add(new UserVM(user));
            }

            return usersVM;
        }

        public IEnumerable<string> GetRoles()
        {
            var roles = Role.List();

            return roles.Select(x => x.Name);
        }
    }
}