﻿using MediatR;

namespace Identity.API.Application.Commands
{
    public class DeleteUserCd : IRequest<bool>
    {
        public int IdUser { get; set; }


        public DeleteUserCd(int idUser)
        {
            IdUser = idUser;
        }
    }
}