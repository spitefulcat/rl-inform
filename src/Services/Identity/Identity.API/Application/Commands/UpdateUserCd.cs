﻿using MediatR;
using System.Collections.Generic;

namespace Identity.API.Application.Commands
{
    public class UpdateUserCd : IRequest<bool>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public IEnumerable<string> Roles { get; set; }
    }
}