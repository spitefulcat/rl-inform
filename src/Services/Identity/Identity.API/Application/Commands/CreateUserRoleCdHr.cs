﻿using Identity.Domain.AggregatesModel.RoleAggregate;
using Identity.Domain.AggregatesModel.UserRoleAggregate;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Identity.API.Application.Commands
{
    public class CreateUserRoleCdHr : IRequestHandler<CreateUserRoleCd, bool>
    {
        private readonly ILogger<CreateUserRoleCdHr> _logger;
        private readonly IUserRoleRepository _userRoleRepository;

        public CreateUserRoleCdHr(ILogger<CreateUserRoleCdHr> logger, IUserRoleRepository userRoleRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userRoleRepository = userRoleRepository ?? throw new ArgumentNullException(nameof(userRoleRepository));
        }

        public async Task<bool> Handle(CreateUserRoleCd message, CancellationToken cancellationToken)
        {
            var idUser = message.IdUser;

            foreach (var role in message.Roles)
            {
                _logger.LogInformation("----- Creating UserRole - IdUser: {@idUser} - Role: {@Role}", idUser, role);

                _userRoleRepository.Create(new UserRole(Role.FromName(role).Id, idUser));
            }

            return await _userRoleRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
        }
    }
}