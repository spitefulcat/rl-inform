﻿using Authentication.Helpers;
using Identity.Domain.AggregatesModel.UserAggregate;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Identity.API.Application.Commands
{
    public class CreateUserCdHr : IRequestHandler<CreateUserCd, bool>
    {
        private readonly ILogger<CreateUserCdHr> _logger;
        private readonly IUserRepository _userRepository;

        public CreateUserCdHr(ILogger<CreateUserCdHr> logger, IUserRepository userRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task<bool> Handle(CreateUserCd message, CancellationToken cancellationToken)
        {
            var user = (User)message;
            user.PasswordHash = PasswordHasher.GetHash(message.Password);

            _logger.LogInformation($"----- Creating User - Login: {user.Login}, Name: {user.Name}, Email: {user.Email}");

            _userRepository.Create(user);

            return await _userRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken);
        }
    }
}