﻿using Identity.Domain.AggregatesModel.UserAggregate;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Identity.API.Application.Commands
{
    public class DeleteUserCdHr : IRequestHandler<DeleteUserCd, bool>
    {
        private readonly ILogger<DeleteUserCdHr> _logger;
        private readonly IUserRepository _userRepository;

        public DeleteUserCdHr(ILogger<DeleteUserCdHr> logger, IUserRepository userRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task<bool> Handle(DeleteUserCd message, CancellationToken cancellationToken)
        {
            var idUser = message.IdUser;

            _logger.LogInformation($"----- Removing user - IdUser: {idUser}");
             
            await _userRepository.Delete(idUser);

            return await _userRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken);
        }
    }
}