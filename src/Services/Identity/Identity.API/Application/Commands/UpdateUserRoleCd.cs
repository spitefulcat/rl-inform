﻿using MediatR;
using System.Collections.Generic;

namespace Identity.API.Application.Commands
{
    public class UpdateUserRoleCd : IRequest<bool>
    {
        public int IdUser { get; set; }

        public IEnumerable<string> Roles { get; set; }


        public UpdateUserRoleCd(int idUser, IEnumerable<string> roles)
        {
            IdUser = idUser;
            Roles = roles;
        }
    }
}