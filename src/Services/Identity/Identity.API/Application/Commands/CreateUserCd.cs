﻿using Identity.Domain.AggregatesModel.RoleAggregate;
using Identity.Domain.AggregatesModel.UserAggregate;
using MediatR;
using System.Collections.Generic;

namespace Identity.API.Application.Commands
{
    public class CreateUserCd : IRequest<bool>
    {
        public string Login { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public IEnumerable<string> Roles { get; set; }


        public static explicit operator User(CreateUserCd command)
        {
            return new User
            {
                Login = command.Login,
                Name = command.Name,
                Email = command.Email
            };
        }
    }
}