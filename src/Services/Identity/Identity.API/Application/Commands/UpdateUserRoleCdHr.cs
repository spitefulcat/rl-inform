﻿using Identity.Domain.AggregatesModel.RoleAggregate;
using Identity.Domain.AggregatesModel.UserRoleAggregate;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Identity.API.Application.Commands
{
    public class UpdateUserRoleCdHr : IRequestHandler<UpdateUserRoleCd, bool>
    {
        private readonly ILogger<UpdateUserRoleCdHr> _logger;
        private readonly IUserRoleRepository _userRoleRepository;

        public UpdateUserRoleCdHr(ILogger<UpdateUserRoleCdHr> logger, IUserRoleRepository userRoleRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userRoleRepository = userRoleRepository ?? throw new ArgumentNullException(nameof(userRoleRepository));
        }

        public async Task<bool> Handle(UpdateUserRoleCd message, CancellationToken cancellationToken)
        {
            var idUser = message.IdUser;
            var userRoles = new List<UserRole>();

            _logger.LogInformation("----- Removing UserRoles - IdUser: {@idUser}", idUser);

            _userRoleRepository.Delete(idUser);

            var result = await _userRoleRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            if (!result)
            {
                return result;
            }

            if (message.Roles == null)
            {
                return true;
            }

            foreach (var role in message.Roles)
            {
                _logger.LogInformation("----- Updating UserRole - IdUser: {@idUser} - Role: {@Role}", idUser, role);
               
                userRoles.Add(new UserRole(Role.FromName(role).Id, idUser));
            }

            _userRoleRepository.CreateAll(userRoles);

            return await _userRoleRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
        }
    }
}