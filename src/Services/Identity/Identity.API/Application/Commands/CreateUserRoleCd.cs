﻿using Identity.Domain.AggregatesModel.RoleAggregate;
using MediatR;
using System.Collections.Generic;

namespace Identity.API.Application.Commands
{
    public class CreateUserRoleCd : IRequest<bool>
    {
        public int IdUser { get; set; }

        public IEnumerable<string> Roles { get; set; }


        public CreateUserRoleCd(int idUser, IEnumerable<string> roles)
        {
            IdUser = idUser;
            Roles = roles;
        }
    }
}