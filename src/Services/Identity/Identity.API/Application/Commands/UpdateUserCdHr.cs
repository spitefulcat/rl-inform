﻿using Identity.Domain.AggregatesModel.UserAggregate;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Identity.API.Application.Commands
{
    public class UpdateUserCdHr : IRequestHandler<UpdateUserCd, bool>
    {
        private readonly ILogger<UpdateUserCdHr> _logger;
        private readonly IUserRepository _userRepository;

        public UpdateUserCdHr(
            ILogger<UpdateUserCdHr> logger,
            IUserRepository userRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task<bool> Handle(UpdateUserCd message, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetById(message.Id);

            user.Name = message.Name;
            user.Email = message.Email;

            _logger.LogInformation($"----- Updating User - IdUser: {user.Id}, Name: {user.Name}, Email: {user.Email}");

            _userRepository.Update(user);

            return await _userRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken);
        }
    }
}