import { RouterModule, Routes } from '@angular/router';

import { ErrorsComponent } from './errors/errors.component';
import { NgModule } from '@angular/core';

const appRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('./identity/components/accounts/accounts.module').then(m => m.AccountsModule)
  },
  { path: '**', component: ErrorsComponent, data: { error: 404 } }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
'';
