import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { User } from '../models/user';
import { UserVM } from '../models/user-vm';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private identityUrl: string = 'http://localhost:5000/api/v1/';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };

  constructor(private http: HttpClient) {}

  GetUser(idUser: number): Observable<UserVM> {
    return this.http.get<UserVM>(this.identityUrl + 'accounts/user/' + idUser);
  }

  GetUsers(): Observable<UserVM[]> {
    return this.http.get<UserVM[]>(this.identityUrl + 'accounts/users');
  }

  AddUser(user: User): Observable<UserVM> {
    var body = JSON.stringify(user);

    return this.http.post<UserVM>(this.identityUrl + 'accounts/create', body, this.httpOptions);
  }

  DeleteUser(idUser: number): Observable<UserVM> {
    return this.http.delete<UserVM>(this.identityUrl + 'accounts/delete/' + idUser);
  }

  EditUser(user: UserVM): Observable<UserVM> {
    var body = JSON.stringify(user);

    return this.http.put<UserVM>(this.identityUrl + 'accounts/update/' + user.id, body, this.httpOptions);
  }

  GetRoles(): Observable<string[]> {
    return this.http.get<string[]>(this.identityUrl + 'accounts/roles');
  }
}
