export class UserVM {
  id: number;
  login: string;
  email: string;
  name: string;
  roles: string[];

  constructor() {
    this.login = '';
    this.email = '';
    this.name = '';
    this.roles = [];
  }
}
