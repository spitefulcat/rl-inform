import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { UserAddComponent } from './user-add/user-add.component';
import { UserComponent } from './user/user.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UsersComponent } from './users/users.component';

const accountsRoutes: Routes = [
  { path: 'accounts/user/:id', component: UserComponent },
  { path: 'accounts/users', component: UsersComponent },
  { path: 'accounts/add', component: UserAddComponent },
  { path: 'accounts/edit/:id', component: UserEditComponent },
  { path: '', redirectTo: '/accounts/users', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(accountsRoutes)],
  exports: [RouterModule]
})
export class AccountsRoutingModule {}
