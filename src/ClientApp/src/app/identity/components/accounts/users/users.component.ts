import { Component, OnInit } from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { UserService } from 'src/app/identity/services/user.service';
import { UserVM } from 'src/app/identity/models/user-vm';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  usersDS = new MatTableDataSource<UserVM>();
  displayedColumns = ['login', 'name', 'roles', 'action'];

  constructor(private router: Router, private userService: UserService) {}

  ngOnInit(): void {
    this.GetUsers();
  }

  GetUsers(): void {
    this.userService.GetUsers().subscribe((users: UserVM[]) => {
      this.usersDS.data = users;
    });
  }

  onCreate(): void {
    this.router.navigate(['accounts/add']);
  }

  onDelete(idUser: number): void {
    this.userService.DeleteUser(idUser).subscribe(data => {
      this.GetUsers();
    });
  }

  onSelectRow(idUser: number): void {
    this.router.navigate(['accounts/user/', idUser]);
  }
}
