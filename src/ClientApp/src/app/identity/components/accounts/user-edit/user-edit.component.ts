import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { UserService } from 'src/app/identity/services/user.service';
import { UserVM } from 'src/app/identity/models/user-vm';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
  idUser: number = 0;
  user: UserVM = new UserVM();
  roles: string[] = [];

  constructor(private activateRoute: ActivatedRoute, private router: Router, private userService: UserService) {
    const idParam = 'id';
    if (this.activateRoute.snapshot.params[idParam]) {
      this.idUser = this.activateRoute.snapshot.params[idParam];
    }
  }

  ngOnInit(): void {
    this.GetUser();
  }

  GetRoles(chosenRoles: string[]): void {
    this.userService.GetRoles().subscribe((roles: string[]) => {
      this.roles = roles;

      this.roles = this.roles.filter(function(item) {
        return !chosenRoles.includes(item);
      });
    });
  }

  GetUser(): void {
    this.userService.GetUser(this.idUser).subscribe((user: UserVM) => {
      this.user = user;
      this.GetRoles(user.roles);
    });
  }

  onRoleAdd(role: string) {
    this.roles = this.roles.filter(x => x != role);

    this.user.roles.push(role);
    this.user.roles.sort((a, b) => {
      if (a < b) return -1;
      else if (a > b) return 1;
      else return 0;
    });
  }

  onRoleDelete(role: string) {
    this.user.roles = this.user.roles.filter(x => x != role);

    this.roles.push(role);
    this.roles.sort((a, b) => {
      if (a < b) return -1;
      else if (a > b) return 1;
      else return 0;
    });
  }

  onEdit() {
    this.userService.EditUser(this.user).subscribe((user: UserVM) => {
      this.router.navigate(['accounts/user/', user.id]);
    });
  }
}
