import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { User } from 'src/app/identity/models/user';
import { UserService } from 'src/app/identity/services/user.service';
import { UserVM } from 'src/app/identity/models/user-vm';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {
  chosenRoles: string[] = [];
  roles: string[] = [];
  user: User = new User();

  constructor(private router: Router, private userService: UserService) {}

  ngOnInit(): void {
    this.GetRoles();
  }

  GetRoles(): void {
    this.userService.GetRoles().subscribe((roles: string[]) => {
      this.roles = roles;
    });
  }

  onCreate(): void {
    this.user.roles = this.chosenRoles;
    this.userService.AddUser(this.user).subscribe((user: UserVM) => {
      this.router.navigate(['accounts/user/', user.id]);
    });
  }

  onRoleAdd(role: string) {
    this.roles = this.roles.filter(x => x != role);

    this.chosenRoles.push(role);
    this.chosenRoles.sort((a, b) => {
      if (a < b) return -1;
      else if (a > b) return 1;
      else return 0;
    });
  }

  onRoleDelete(role: string) {
    this.chosenRoles = this.chosenRoles.filter(x => x != role);

    this.roles.push(role);
    this.roles.sort((a, b) => {
      if (a < b) return -1;
      else if (a > b) return 1;
      else return 0;
    });
  }
}
