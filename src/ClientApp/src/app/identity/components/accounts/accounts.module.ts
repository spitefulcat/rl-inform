import { AccountsRoutingModule } from './accounts-routing.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared.module';
import { UserAddComponent } from './user-add/user-add.component';
import { UserComponent } from './user/user.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UsersComponent } from './users/users.component';

@NgModule({
  declarations: [UserComponent, UsersComponent, UserAddComponent, UserEditComponent],
  imports: [CommonModule, AccountsRoutingModule, SharedModule]
})
export class AccountsModule {}
