import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { UserService } from 'src/app/identity/services/user.service';
import { UserVM } from 'src/app/identity/models/user-vm';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  idUser: number = 0;
  user: UserVM = new UserVM();

  constructor(private activateRoute: ActivatedRoute, private router: Router, private userService: UserService) {
    const idParam = 'id';
    if (this.activateRoute.snapshot.params[idParam]) {
      this.idUser = this.activateRoute.snapshot.params[idParam];
    }
  }

  ngOnInit(): void {
    this.GetUser();
  }

  GetUser(): void {
    this.userService.GetUser(this.idUser).subscribe((user: UserVM) => {
      this.user = user;
    });
  }

  onEdit(idUser: number): void {
    console.log('idUser: ', idUser);
    this.router.navigate(['accounts/edit/', idUser]);
  }
}
